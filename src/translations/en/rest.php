<?php
/**
 * Rest plugin for Craft CMS 3.x
 *
 * Wrapper of Yii Rest for Craft CMS.
 *
 * @link      https://wesrice.com
 * @copyright Copyright (c) 2017 Wes Rice
 */

/**
 * Rest en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t()`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Wes Rice
 * @package   Rest
 * @since     0.0.0
 */
return [
    'plugin loaded' => 'plugin loaded',
];
